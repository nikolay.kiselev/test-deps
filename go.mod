module gitlab.com/nikolay.kiselev/test-deps

go 1.16

require (
	github.com/golang-migrate/migrate/v4 v4.15.2
	github.com/kiselev-nikolay/go-test-docker-dependencies v0.0.0-20210513133420-9d70db7cbb6c
	github.com/pkg/errors v0.9.1
)

require (
	github.com/go-pg/pg/v10 v10.10.6
	mellium.im/sasl v0.3.0 // indirect
)
