package deps

import (
	"fmt"
	"path/filepath"

	"github.com/go-pg/pg/v10"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/kiselev-nikolay/go-test-docker-dependencies/testdep"
	"github.com/pkg/errors"
)

type PostgresContainer struct {
	DB   *pg.DB
	URL  string
	Stop func()
	Err  error
}

func MustCreateDB(migrationFiles string) (*PostgresContainer, error) {
	dbConn := &PostgresContainer{
		Stop: func() {},
	}

	dbPort, err := testdep.FindFreePort()
	if err != nil {
		return nil, errors.Wrap(err, "failed to find free port")
	}

	dockerPg := testdep.Postgres{
		Port:     dbPort,
		User:     "test",
		Password: "test",
		Database: "test",
	}
	stop, err := dockerPg.Run(10)
	if err != nil {
		return nil, errors.Wrap(err, "failed to run postgres database in docker")
	}

	dbConn.URL = fmt.Sprintf("postgres://%s:%s@localhost:%d/%s?sslmode=disable",
		dockerPg.User, dockerPg.Password,
		dockerPg.Port, dockerPg.Database)
	dbConn.Stop = func() {
		if stop != nil {
			if errStop := stop(); errStop != nil {
				dbConn.Err = errors.Wrap(errStop, "failed to stop container")
			}
		}
	}

	migrationFiles, err = filepath.Abs(migrationFiles)
	if err != nil {
		return nil, errors.Wrap(err, "failed to locate migrations folder")
	}

	m, err := migrate.New(
		"file://"+migrationFiles,
		dbConn.URL)
	if err != nil {
		return nil, errors.Wrap(err, "failed to setup new migrator")
	}

	err = m.Up()
	if err != nil {
		return nil, errors.Wrap(err, "failed to run migrations")
	}

	pgOpt, err := pg.ParseURL(dbConn.URL)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse url")
	}
	dbConn.DB = pg.Connect(pgOpt)

	return dbConn, nil
}
